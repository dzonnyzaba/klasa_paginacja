<?php

class Paginacja{
	private $ilosc_wszystkich_rekordow;
	private $url;
	private $zgeta;
	private $ilosc_stron1;
	public function __construct($ilosc_wszystkich_rekordow, $url, $zgeta){
		$this->ilosc_wszystkich_rekordow = $ilosc_wszystkich_rekordow;
		$this->url = $url;
		$this->zgeta = $zgeta;
		$this->ilosc_stron1 = ceil($ilosc_wszystkich_rekordow/20);
	}
	
	public function oblicz_ilosc_widocznych_linkow(){
		//$ilestron = ceil($this->ilosc_wszystkich_rekordow/20);
		if($this->ilosc_stron1>10){
			$ile_stron_min = 10;
		}else{
			$ile_stron_min = $this->ilosc_stron1;
		}
		return $ile_stron_min;
	}
	
	public function arrow_w_lewo(){
		if($this->zgeta>1){
			echo $this->url.($this->zgeta-1)."'><</a> ";
		}
		if($this->zgeta>6){
			echo $this->url."1'>1...</a> ";
		}
	}
	
	public function arrow_w_prawo(){
		if($this->zgeta<($this->ilosc_stron1-5)){
			echo $this->url.$this->ilosc_stron1."'>...".$this->ilosc_stron1."</a> ";
		}
		if($this->zgeta<$this->ilosc_stron1){
			echo $this->url.($this->zgeta+1)."'>></a> ";
		}
	}
	
	public function wyswietl_linka($liczba, $bool){
		if($bool==true){
			echo "<font size='4'><b>".$this->url.$liczba."'>".$liczba."</a></b></font> ";
		}else{
			echo $this->url.$liczba."'>$liczba</a> ";
		}
	}
	
	public function sprawdzaj_warunki(){
		$this->arrow_w_lewo();
		$ilosc_widocznych_linkow = $this->oblicz_ilosc_widocznych_linkow();
		if($this->zgeta==''){
			for($i=1; $i<$ilosc_widocznych_linkow; $i++){
				if($i==1){
					$this->wyswietl_linka($i, true);
				}else{
					$this->wyswietl_linka($i, false);
				}
			}
		}//koniec ifa pusty get
		else{
			if(($this->zgeta>5) && ($this->zgeta<($this->ilosc_stron1-5))){
				for($i=($this->zgeta-5); $i<($this->zgeta+6); $i++){
					if($i==$this->zgeta){
						$this->wyswietl_linka($i, true);
					}else{
						$this->wyswietl_linka($i, false);
					}
				}//koniec for
			}elseif($this->zgeta<=5){
				for($i=1; $i<=$ilosc_widocznych_linkow; $i++){
					if($i==$this->zgeta){
						$this->wyswietl_linka($i, true);
					}else{
						$this->wyswietl_linka($i, false);
					}
				}
			}else{
				for($i=($this->zgeta-5); $i<=$this->ilosc_stron1; $i++){
					if($i==$this->zgeta){
						$this->wyswietl_linka($i, true);
					}else{
						$this->wyswietl_linka($i, false);
					}
				}
			}
		}//koniec else od pusty get
		$this->arrow_w_prawo();
	}//koniec sprawdzaj_warunki()
	
	public function ustaw_limit(){
		if(!isset($this->zgeta)){
			$l = "LIMIT 0,20";
		}else{
			$pocz = ($this->zgeta*20)-20;
			$l = "LIMIT $pocz,20";
		}
		return $l;
	}

}